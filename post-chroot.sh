#!/bin/sh

ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
locale-gen
hwclock --systohc

systemctl enable systemd-timesyncd
systemctl enable iwd
systemctl enable dhcpcd
systemctl enable sshd
systemctl enable rsyncd
systemctl enable ufw
systemctl enable systemd-vconsole-setup

mkinitcpio -P

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=ARCH
grub-mkconfig -o /boot/grub/grub.cfg
clear

printf 'Enter your new username: '
read -r newuser
printf 'Enter your new password: '
read -r newpass

printf 'Creating new user: %s:wheel' "${newuser}"
printf '%s:%s' "${newuser}" "${newpass}" | chpasswd
clear

printf 'Symlinking dash to sh...\n'
ln -sf /usr/bin/dash /usr/bin/sh
printf "You're ready to reboot!\n"