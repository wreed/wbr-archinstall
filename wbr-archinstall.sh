#!/bin/sh

printf 'Connect to your wifi\n'
sleep 2

iwctl

printf 'Waiting a few seconds for connection...\n'
sleep 1;

printf '5.. '; sleep 1; printf '4.. '; sleep 1; printf '3.. '; sleep 1; printf '2.. '; sleep 1; printf '1.. \n'; sleep 1;

pacman -Sy && pacman -S archlinux-keyring && pacman-key --init

printf "Setting console font...\n";

setfont ter-132b

printf "Fix timezone/ntp...\n"
timedatectl set-timezone America/Chicago
timedatectl set-ntp true

# SECTION: disks

disk="/dev/nvme0n1"
boot="${disk}p1"
swap="${disk}p2"
root="${disk}p3"

printf 'Preparing partitions on %s\n' $disk

sgdisk $disk --zap-all && wipefs --all --force $disk

sgdisk $disk --clear

sgdisk $disk --new=1:2048:2099199 --typecode=1:ef00
sgdisk $disk --new=2:2099200:37750783 --typecode=2:8200
sgdisk $disk --largest-new=3 --typecode=3:8300

mkfs.vfat -c -F 32 -n EFI $boot
mkswap --label SWAP --check --endianness=native $swap
mkfs.btrfs --force --label MAIN --checksum blake2 $root

clear;

mount $root /mnt

btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@var
btrfs subvolume create /mnt/@local
btrfs subvolume create /mnt/@backup

umount /mnt
mount -t btrfs -o noatime,compress=lzo,ssd,subvol=@ $root /mnt;

mkdir -p /mnt/boot
mkdir -p /mnt/home
mkdir -p /mnt/var
mkdir -p /mnt/local
mkdir -p /mnt/backup

mount -t btrfs -o noatime,compress=lzo,ssd,subvol=@home   $root /mnt/home
mount -t btrfs -o noatime,compress=lzo,ssd,subvol=@var    $root /mnt/var
mount -t btrfs -o noatime,compress=lzo,ssd,subvol=@local  $root /mnt/local
mount -t btrfs -o noatime,compress=lzo,ssd,subvol=@backup $root /mnt/backup

mount $boot /mnt/boot
swapon $swap

tee -a /etc/pacman.conf <<- 'EOF'

[community]
Include = /etc/pacman.d/mirrorlist

[multilib]
Include = /etc/pacman.d/mirrorlist

EOF
sed -i 's|#Color|Color\nILoveCandy\nParallelDownloads = 3\n|' /etc/pacman.conf

clear
printf 'Running reflector to get better mirrors\n'
reflector \
  --protocol https \
  --country US \
  --latest 40 \
  --fastest 10 \
  --sort rate \
  --save /etc/pacman.d/mirrorlist

pacman -Syy

pacstrap -PK /mnt - < "$(pwd)/packages.list"

genfstab -L /mnt >> /mnt/etc/fstab

tee -a /mnt/etc/locale.gen <<- 'EOF'

en_US.UTF-8 UTF-8
en_US ISO-8859-1

EOF

echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf

tee -a /mnt/etc/vconsole.conf <<- 'EOF'

KEYMAP=us
FONT_MAP=8859-1
FONT=ter-132b

EOF

cp post-chroot.sh /mnt/post-chroot.sh
clear

printf "Alright, you're ready to chroot!\n"
printf "you can run post-chroot.sh when you're there.\n"